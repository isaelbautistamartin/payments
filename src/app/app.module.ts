import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TheHeader } from './components/header/header.component';
import { Payment } from './components/payment/payment.component';
import { PaymentForm } from './components/paymentForm/paymentForm.component';

@NgModule({
  declarations: [
    AppComponent,
    TheHeader,
    Payment,
    PaymentForm,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
