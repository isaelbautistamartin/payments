"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.AppComponent = void 0;
var core_1 = require("@angular/core");
var shortid_1 = require("shortid");
var paymentForm_component_1 = require("./components/paymentForm/paymentForm.component");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.payments = [];
        this.total = 0;
    }
    AppComponent.prototype.editPayment = function ($event) {
        var editedPaymentId = $event.paymentIdToEdit;
        var editedPayment = $event.payment;
        var editedPayments = this.payments.map(function (payment) {
            if (payment.id === editedPaymentId) {
                payment = __assign(__assign({}, payment), { amount: editedPayment.amount, date: editedPayment.date, description: editedPayment.description });
            }
            return payment;
        });
        this.payments = __spreadArrays(editedPayments);
    };
    AppComponent.prototype.addPayment = function ($event) {
        var payment = $event.payment;
        payment.id = shortid_1["default"].generate();
        this.payments = __spreadArrays(this.payments, [payment]);
    };
    AppComponent.prototype.calcTotal = function () {
        this.total = this.payments.reduce(function (acc, payment) { return (acc + payment.amount); }, 0);
    };
    AppComponent.prototype.deletePayment = function ($event) {
        var payment_id = $event.payment_id;
        this.payments = this.payments.filter(function (payment) { return payment.id !== payment_id; });
        this.calcTotal();
    };
    AppComponent.prototype.setPaymentToEdit = function ($event) {
        var payment_id = $event.payment_id;
        var payment = this.payments.filter(function (payment) { return payment.id === payment_id; });
        this.childcomp.setPaymentToEdit(payment);
    };
    __decorate([
        core_1.ViewChild(paymentForm_component_1.PaymentForm)
    ], AppComponent.prototype, "childcomp");
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.sass'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
