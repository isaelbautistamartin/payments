import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'the-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class TheHeader {

  @Input() total: number;

}
