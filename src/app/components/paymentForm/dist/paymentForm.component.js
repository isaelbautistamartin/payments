"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.PaymentForm = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var PaymentForm = /** @class */ (function () {
    function PaymentForm(formBuilder) {
        this.formBuilder = formBuilder;
        this.addPayment = new core_1.EventEmitter();
        this.calcTotal = new core_1.EventEmitter();
        this.editPayment = new core_1.EventEmitter();
        this.isSubmitted = false;
        this.payments = [];
        this.total = 0;
        this.editFlag = false;
        this.editPaymentId = '';
        this.checkoutForm = this.formBuilder.group({
            amount: [
                '',
                [
                    forms_1.Validators.required,
                    this.nonZero,
                    forms_1.Validators.pattern(/^\-?[0-9]+(\.[0-9]{1,2})?$/)
                ]
            ],
            date: [
                '',
                [
                    forms_1.Validators.required,
                    this.isValidDate
                ]
            ],
            description: [
                '',
                forms_1.Validators.maxLength(255)
            ]
        });
    }
    PaymentForm.prototype.nonZero = function (amount) {
        if (amount.value <= 0 && amount.value) {
            return { nonZero: true };
        }
        return null;
    };
    PaymentForm.prototype.isValidDate = function (date) {
        if (date.value) {
            var sevenDaysAgo = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
            sevenDaysAgo.setHours(0, 0, 0, 0);
            var actualDate = new Date(date.value);
            actualDate.setDate(actualDate.getDate() + 1);
            actualDate.setHours(0, 0, 0, 0);
            var invalidDate = actualDate < sevenDaysAgo;
            return invalidDate ? { isValidDate: invalidDate } : null;
        }
    };
    Object.defineProperty(PaymentForm.prototype, "formControls", {
        get: function () { return this.checkoutForm.controls; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PaymentForm.prototype, "amount", {
        get: function () { return this.checkoutForm.get('amount'); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PaymentForm.prototype, "date", {
        get: function () { return this.checkoutForm.get('date'); },
        enumerable: false,
        configurable: true
    });
    PaymentForm.prototype.onSubmit = function () {
        this.isSubmitted = true;
        if (this.checkoutForm.invalid) {
            return;
        }
        var payment = this.checkoutForm.value;
        if (this.editFlag) {
            this.edit(payment);
        }
        else {
            this.addPayment.emit({ payment: payment });
        }
        this.checkoutForm.reset();
        this.calcTotal.emit();
        this.isSubmitted = false;
    };
    PaymentForm.prototype.edit = function (payment) {
        this.editPayment.emit({ payment: payment, paymentIdToEdit: this.editPaymentId });
        this.editFlag = false;
    };
    PaymentForm.prototype.setPaymentToEdit = function (payment) {
        this.editPaymentId = payment[0].id;
        this.editFlag = true;
        this.checkoutForm.controls['amount'].setValue(payment[0].amount);
        this.checkoutForm.controls['date'].setValue(payment[0].date);
        this.checkoutForm.controls['description'].setValue(payment[0].description);
    };
    __decorate([
        core_1.Output("addPayment")
    ], PaymentForm.prototype, "addPayment");
    __decorate([
        core_1.Output("calcTotal")
    ], PaymentForm.prototype, "calcTotal");
    __decorate([
        core_1.Output("editPayment")
    ], PaymentForm.prototype, "editPayment");
    PaymentForm = __decorate([
        core_1.Component({
            selector: 'payment-form',
            templateUrl: './paymentForm.component.html',
            styleUrls: ['./paymentForm.component.sass']
        })
    ], PaymentForm);
    return PaymentForm;
}());
exports.PaymentForm = PaymentForm;
