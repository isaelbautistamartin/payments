import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'payment-form',
  templateUrl: './paymentForm.component.html',
  styleUrls: ['./paymentForm.component.sass']
})
export class PaymentForm {

  @Output("addPayment") addPayment: EventEmitter<any> = new EventEmitter();
  @Output("calcTotal") calcTotal: EventEmitter<any> = new EventEmitter();
  @Output("editPayment") editPayment: EventEmitter<any> = new EventEmitter();
  

  checkoutForm: FormGroup;
  isSubmitted  =  false;
  payments = [];
  total = 0;
  editFlag = false;
  editPaymentId = '';

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.checkoutForm = this.formBuilder.group({
      amount: [
        '',
        [
          Validators.required,
          this.nonZero,
          Validators.pattern(/^\-?[0-9]+(\.[0-9]{1,2})?$/)  
        ]
      ],
      date: [
        '',
        [
          Validators.required,
          this.isValidDate
        ]
      ],
      description: [
        '', 
        Validators.maxLength(255)
      ],
    });
  }

  nonZero(amount: FormControl) {
    if (amount.value <= 0 && amount.value) {
      return { nonZero: true };
    }
    
    return null
  }

  isValidDate(date: FormControl) {
    if (date.value) {

      let sevenDaysAgo = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
      sevenDaysAgo.setHours(0,0,0,0);
      let actualDate = new Date(date.value)
      actualDate.setDate(actualDate.getDate() + 1);
      actualDate.setHours(0,0,0,0);
  
      let invalidDate = actualDate < sevenDaysAgo; 

      return invalidDate ? { isValidDate: invalidDate }  : null;
      
    }

  }

  get formControls() { return this.checkoutForm.controls; }

  get amount() { return this.checkoutForm.get('amount'); }

  get date() { return this.checkoutForm.get('date'); }

  onSubmit() {
    this.isSubmitted = true;
    
    if(this.checkoutForm.invalid){
      return;
    }

    let payment = this.checkoutForm.value;

    if (this.editFlag) {
      this.edit(payment);
    } else {
      this.addPayment.emit({ payment });
    }
    
    this.checkoutForm.reset();

    this.calcTotal.emit();

    this.isSubmitted = false;
  }

  edit(payment) {
    this.editPayment.emit({payment, paymentIdToEdit: this.editPaymentId});
    this.editFlag = false;
  }

  setPaymentToEdit(payment) {
    
    this.editPaymentId = payment[0].id;
    this.editFlag = true;

    this.checkoutForm.controls['amount'].setValue(payment[0].amount)
    this.checkoutForm.controls['date'].setValue(payment[0].date)
    this.checkoutForm.controls['description'].setValue(payment[0].description)
    
  }

}
