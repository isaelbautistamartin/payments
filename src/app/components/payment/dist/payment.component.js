"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Payment = void 0;
var core_1 = require("@angular/core");
var Payment = /** @class */ (function () {
    function Payment() {
        this.deletePayment = new core_1.EventEmitter();
        this.setPaymentToEdit = new core_1.EventEmitter();
    }
    Payment.prototype["delete"] = function (payment) {
        this.deletePayment.emit({ payment_id: payment.id });
    };
    Payment.prototype.edit = function (payment_id) {
        this.setPaymentToEdit.emit({ payment_id: payment_id });
    };
    __decorate([
        core_1.Input('payment')
    ], Payment.prototype, "payment");
    __decorate([
        core_1.Output("deletePayment")
    ], Payment.prototype, "deletePayment");
    __decorate([
        core_1.Output("setPaymentToEdit")
    ], Payment.prototype, "setPaymentToEdit");
    Payment = __decorate([
        core_1.Component({
            selector: 'payment',
            templateUrl: './payment.component.html',
            styleUrls: ['./payment.component.sass'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], Payment);
    return Payment;
}());
exports.Payment = Payment;
