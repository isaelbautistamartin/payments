import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class Payment {

  @Input('payment') payment;
  @Output("deletePayment") deletePayment: EventEmitter<any> = new EventEmitter();
  @Output("setPaymentToEdit") setPaymentToEdit: EventEmitter<any> = new EventEmitter();

  delete(payment) {
    this.deletePayment.emit({payment_id:payment.id});
  }

  edit(payment_id) {
    this.setPaymentToEdit.emit({ payment_id });
  }

}
