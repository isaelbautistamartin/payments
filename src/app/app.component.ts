import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import shortid from 'shortid';

import { PaymentForm } from './components/paymentForm/paymentForm.component'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {

  payments = [];
  total = 0;

  @ViewChild(PaymentForm) childcomp: PaymentForm;
  

  editPayment($event) {
    let editedPaymentId = $event.paymentIdToEdit;
    let editedPayment = $event.payment;

    let editedPayments = this.payments.map(payment => {
      if (payment.id === editedPaymentId) {
        payment = { 
          ...payment,
          amount: editedPayment.amount,
          date: editedPayment.date,
          description: editedPayment.description,
        }
      }

      return payment;
    });

    this.payments = [...editedPayments];

  }

  addPayment($event: any) {
    let payment = $event.payment;
    payment.id = shortid.generate(); 

    this.payments = [...this.payments, payment];
  }

  calcTotal() {
    this.total = this.payments.reduce((acc, payment) => ( acc + payment.amount ), 0);
  }

  deletePayment($event: any) {
    let payment_id = $event.payment_id;
    this.payments = this.payments.filter(payment => payment.id !== payment_id);

    this.calcTotal();
  }

  setPaymentToEdit($event: any) {
    let payment_id = $event.payment_id;
    let payment = this.payments.filter(payment => payment.id === payment_id);
    this.childcomp.setPaymentToEdit(payment);
    
  }

}
